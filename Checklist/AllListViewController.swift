//
//  AllListViewController.swift
//  Checklist
//
//  Created by Gruszkovsky on 10.04.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class AllListViewController: UITableViewController {

    var lists: [Checklist]
    
    required init?(coder aDecoder: NSCoder) {
        lists = [Checklist]()
        super.init(coder: aDecoder)
        var list = Checklist()
        list.name = "Birthdays"
        lists.append(list)
        list = Checklist()
        list.name = "Groceries"
        lists.append(list)
        list = Checklist()
        list.name = "Cool Apps"
        lists.append(list)
        list = Checklist()
        list.name = "To Do"
        lists.append(list)
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = makeCell(for: tableView)
        cell.textLabel!.text = "List \(indexPath.row)"
        return cell
    }
    
    func makeCell(for tableView: UITableView) -> UITableViewCell {
        let cellIdentifier = "Cell"
        if let cell =
            tableView.dequeueReusableCell(withIdentifier: cellIdentifier) {
            return cell
        } else {
            return UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowChecklist", sender: nil)
    }
   
}
